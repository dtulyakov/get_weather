#!/usr/bin/env python3
# coding: utf-8
import sys
import requests
import xml.etree.ElementTree as Xml
from codecs import open
import os.path
import argparse

data_test = """<?xml version="1.0"?>
<data>
    <country name="Тест">Тест
        <rank updated="yes">2</rank>
        <year>2008</year>
        <gdppc>141100</gdppc>
        <neighbor name="Austria" direction="E"/>
        <neighbor name="Switzerland" direction="W"/>
    </country>
    <country name="Singapore">
        <rank updated="yes">5</rank>
        <year>2011</year>
        <gdppc>59900</gdppc>
        <neighbor name="Malaysia" direction="N"/>
    </country>
    <country name="Panama">
        <rank updated="yes">69</rank>
        <year>2011</year>
        <gdppc>13600</gdppc>
        <neighbor name="Costa Rica" direction="W"/>
        <neighbor name="Colombia" direction="E"/>
    </country>
</data>"""

# Раздел с константами #
URL_XML_CITIES  = 'https://pogoda.yandex.ru/static/cities.xml'
URL_XML_WEATHER = 'http://export.yandex.ru/weather-ng/[id_city].xml'

NAME_XML_CITIES = 'cities.xml'

STATUS_REQUEST_OK = 200
########################

# Инициализация параметров консоли
def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('title_city', nargs='?')
    parser.add_argument('days_count', nargs='?')
    return parser

# Запись в файл
def writeToFile(name, text):
    #out = open(name, "w", "utf-8") #windows
    out = open(name, "w")
    out.write(text)
    out.close()

# Обновляем файл со списком городов
def get_xml_cities_file():
    if ( not os.path.exists(NAME_XML_CITIES) ):
        request = requests.get(URL_XML_CITIES)
        if ( request.status_code == STATUS_REQUEST_OK ):
            writeToFile( NAME_XML_CITIES, request.text )
    return NAME_XML_CITIES

parser = createParser()
namespace = parser.parse_args()

file_name = get_xml_cities_file()
